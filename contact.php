<?php include('header.php'); ?>

<!-- Document Title
	============================================= -->
	<title>Connect | SL Kreativez</title>


<?php include('nav.php'); ?>


		<!-- Page Title
		============================================= -->
	
		<div class="promo promo-dark promo-flat promo-full footer-stick"  style="background-color:#0087BD;">
			<div class="container clearfix text-center">
				<h5 style="color:white;">Awesome you're here. Do you have any question for us? Or you just want to know us? Want to share with us or just encourage us? Be sure we can't wait to be in touch.</h5>
			</div>
		</div>	

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

				<h4> Make use of any of our social media platforms for your ease and comfort. We will get back to you with warmth.</h4>

				<!-- Contact Info
					============================================= -->
					<div class="row clear-bottommargin">

						<div class="col-md-3 col-sm-6 bottommargin clearfix">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href="#"><img src="images/location.png"></a>
								</div>
								<h3>Our Headquarters<span class="subtitle"><br>Lagos, Nigeria</span></h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 bottommargin clearfix">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href="#"><img src="images/phone.png"></i></a>
								</div>
								<h3>Speak to Us<span class="subtitle"><br>08012345678</span></h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 bottommargin clearfix">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href="#"><img src="images/email.png">></i></a>
								</div>
								<h3>Send us an Email<span class="subtitle"><br>info@seekerslocus.com</span></h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 bottommargin clearfix">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href="#"><img src="images/company.png"></a>
								</div>
								<h3>Follow Us<span class="subtitle" style="margin-bottom: 6px;">
								<ul style="text-decoration:none; margin: 0 auto; ">
									<li style="display:inline-block;">
										<a href="#"><img src="images/fb.png"></a>
									</li>
									<li style="display:inline-block;">
										<a href="#"><img src="images/tw.png"></a>
									</li>
									<li style="display:inline-block;">
										<a href="#"><img src="images/insta.png"></a>
									</li>
									<li style="display:inline-block;">
										<a href="#"><img src="images/link.png"></a>
									</li>


								</ul></span></h3>

							
							</div>
						</div>

					</div><!-- Contact Info End -->

				</div>
				<div class="clear"></div>
			</div>
		</section>

		<section>
			<img src="images/sl2.jpg">
		</section>

		<section>
			<div style="position: relative; padding-top:60px; padding-bottom:- 100px; overflow: hidden; ">
				<div class="container clearfix">
				<h4>None of these? Then fill our form </h4>

			<!-- Contact Form
				============================================= -->
				<div class="col_half" id="contact">

					<div class="contact-widget">

						<div class="contact-form-result"></div>

						<form class="nobottommargin" id="template-contactform" name="template-contactform" action="http://themes.semicolonweb.com/html/canvas/include/sendemail.php" method="post">

							<div class="form-process"></div>

							<div class="col_one_third">
								<label for="template-contactform-name">Name <small>*</small></label>
								<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />
							</div>

							<div class="col_one_third">
								<label for="template-contactform-email">Email <small>*</small></label>
								<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
							</div>

							<div class="col_one_third col_last">
								<label for="template-contactform-phone">Phone</label>
								<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control" />
							</div>

							<div class="clear"></div>

							<div class="col_full">
								<label for="template-contactform-subject">Subject <small>*</small></label>
								<input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control" />
							</div>

							<div class="clear"></div>

							<div class="col_full">
								<label for="template-contactform-message">Message <small>*</small></label>
								<textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
							</div>

							<div class="col_full hidden">
								<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
							</div>

							<div class="col_full">
								<button name="submit"  style="background-color:#0087BD;" type="submit" id="submit-button" tabindex="5" value="Submit" class="button button-3d nomargin">Submit Comment</button>
							</div>

						</form>
					</div>

				</div><!-- Contact Form End -->

				<!-- Google Map
				============================================= -->
				<div class="col_half col_last hidden-lg hidden-md hidden-sm">

					<!--<section id="google-map" class="gmap" style="height: 410px;"></section> -->
					<iframe  height="300px"  frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJwYCC5iqLOxARy9nDZ6OHntw&key=AIzaSyB73BjGykPoCd9ONfgCH1IhpHQeFGMa88Q"  allowfullscreen>
					</iframe>

				</div><!-- Google Map End -->
				<div class="col_half col_last hidden-xs ">

					<!--<section id="google-map" class="gmap" style="height: 410px;"></section> -->
					<iframe  height="370px" width="520px"  frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJwYCC5iqLOxARy9nDZ6OHntw&key=AIzaSyB73BjGykPoCd9ONfgCH1IhpHQeFGMa88Q"  allowfullscreen>
					</iframe>

				</div><!-- Google Map End -->

				</div>

			</div>

		</section>


<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix" >

					
					<div class="col-md-6 widget subscribe-widget clearfix" style="margin-bottom: 50px;">
						<h4><img src="images/copy2.jpg"> Copyright</h4> 
						<p align="justify">Hi, as you know most of the information and contents on this website are the intellectual properties of creative individuals. It wouldn't be illegal to use their work without  proper request and attribution. 
						To this end, all contents on seekerslocus.com and other information contained are the exclusive property of SL KREATIVEZ and her partners. They can be shared on social media but any other use must come with express permission from us. When used for educational purposes, users must reference us appropriately. Any commercial use must be with a permission of copyright use. We take the right permission when we use contents from third parties, and they’re properly attributed. <br>You can reach us via info@seekerslocus.com, Thank You.
						</p>
						<div class="clearfix"></div>
						<br>
						<h4><strong>Subscribe</strong> to Our Newsletter to get Important News and latest opporrtunities:</h4>
						<div class="widget-subscribe-form-result"></div>
						<form id="widget-subscribe-form" action="http://themes.semicolonweb.com/html/canvas/include/subscribe.php" role="form" method="post" class="nobottommargin">
							<div class="input-group divcenter">
								
								<input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email" style="height:40px;">
								<span class="input-group-btn">
									<button class="button button-3d noleftmargin" style="margin-top:0px;" type="submit">Subscribe</button>
								</span>
							</div>
						</form>
					</div>


					<div class="col-md-6 widget subscribe-widget clearfix" style="margin-top:-5px">
						<h4><img src="images/insta2.jpg"> INSTAGRAM FEEDS </h4>
						<p>
							<iframe src="//users.instush.com/bee-gallery-v2/?cols=5&rows=2&size=small&border=10&round=false&space=4&sl=true&bg=transparent&brd=true&na=false&pin=true&hc=e72476&ltc=3f3f3f&lpc=ffffff&fc=ffffff&user_id=1474819067&username=_ujunwa&sid=-1&susername=-1&tag=-1&stype=mine&t=999999O3mfm-kwleBjgBevNAu0_y4fA-y4hJ8dX5Bx-BUc1Tun1lYnHYIOerZYpdh0sJr5hP59mHe15gs" allowtransparency="true" frameborder="0" scrolling="no"  style="display:block;width:548px;height:224px;border:none;overflow:visible;" ></iframe>
						</p>

						
						<h4>Graphics Designer:<span style="font-size: 13px;"> Samson Ochuko, professional graphics artist and CEO of Phebony Graphics for the Seamless logo and site banner.</span></h4>
						<h4>Images:<span style="font-size: 13px;">pixabay.com, pexels.com for the free to use stock images.</span></h4>
					</div>

				</div><!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div style="margin-bottom:0px; padding-top: 40px; background-color: rgba(0,0,0,0.2);">

				<div class="container clearfix">

					<div class="col_full widget subscribe-widget clearfix" align="center">
						<h4>Copyrights SL Kreativez &copy; 2017 | <span style="font-size: 13px;"> Designed by Rosie Andre (iro3 designs) </h4>
					</div>
	

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="assets/js/jquery.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="assets/js/functions.js"></script>

	<script type="text/javascript" src="assets/js/new.js"></script>

	
<script>jQuery(document).ready(function(e){e("#primary-menu > ul li").find('.new-badge').children("div").append('<span class="label label-danger" style="display:inline-block;margin-left:8px;position:relative;top:-1px;text-transform:none;">New</span>')});</script>

</html>