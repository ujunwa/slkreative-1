<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

	<!-- Google Analytics -->
	<script type="text/javascript" src="assets/js/gtag.js"></script>

	<meta charset="UTF-8">
	<meta name="description" content="SL Kreativez is a social enterprise with interest in developing the next generation of writers and creative geniuses. ">
	<meta name="keywords" content="SL Kreativez, SL, Energizing, creative, Artistry">
	<meta name="author" content="SL Kreativez">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="assets/main.css" type="text/css" />
	<link rel="stylesheet" href="assets/css/fonts/font-awesome.css" type="text/css" />

	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
	
